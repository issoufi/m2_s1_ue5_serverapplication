# Configuration du projet

## ⚒ Près-requis

Pour pouvoir exécuter le projet sans problème. Il est recommandé d'utiliser la même configuration que la nôtre. À savoir :
- Utiliser Java 8
- JAVA EE 7
- NetBeans 8.2

Il faudra aussi avoir les . jar suivants:
- Primefaces 5
- com.lowagie.text-2.1.7

Concernant le navigateur, on vous recommande d'utiliser la dernière version de navigateur Google Chrome, car des modules CSS tel que FlexBox et Grid sont utilisé et risque de ne pas fonctionner correctement sur Safari ou IE.

## ⏬ Télécharger le projet Git et les dépendances

```bash
git clone https://gitlab.com/issoufi/m2_s1_ue5_serverapplication.git projetNiketaAdam
cd projetNiketaAdam
wget http://www.java2s.com/Code/JarDownload/com.lowagie/com.lowagie.text-2.1.7.jar.zip
unzip com.lowagie.text-2.1.7.jar.zip // Vous obtenez le ficher com.lowagie.text-2.1.7.jar
rm -f com.lowagie.text-2.1.7.jar.zip // On supprime le .zip qui est unitile
```

## 🚀 Lancer le projet

1. Lancer NetBeans 8.2 et ouvrer le projer `File > Open Project...`
2. Vérifez dans l'onglet `services` que parmis les bases de données (`databese`) que vous la DB `sample`. Dans le cas contraire créer ce dernier.
3. Ajouter à `TP3BanqueAdamEme-war` la librairie ficher com.lowagie.text-2.1.7.jar. Clic droit sur `Libraries`, puis `Add JAR/Folder` et aller la ou se trouve .jar pour l'ajouter.
4. Ajouter à `TP3BanqueAdamEme-war` la Primefaces 5. Clic droit sur `Libraries` puis `Add Library...`, sélectionner `Primefaces` et ajouter ce dernier.
5. En fin, lancer le projet en faisant un `clean and build` puis un `run`.
