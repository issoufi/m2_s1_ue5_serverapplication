# Rapport de conception

Vous trouverez ici toutes les informations concernant la conception de l'application web.

## Diagramme UML

Voici l'ensemble des entités qu'on a choisi de réalisées pour le projet.

![Diagramme UML](img/screenl-1.png)

## Pages réalisées

Voici une vu d'ensemble des pages réalisées :

![Diagramme UML](img/screenl-0.png)

Comme vous pouvez le constater les pages sont séparé en deux grandes sections. La première section regroupe les pages de consultation. Par exemple, on peut visualiser l'ensemble de ses comptes bancaires, la liste des clients, la liste des conseillers, etc.

La deuxième section regroupe tout ce qui est gestion (création et opération bancaire). On peut par exemple créer un compte, effectuer un virement, etc.

## Fonctionnalités implémentées
- [x] Ajout/Création d'entités métiers : Comptes Bancaires, Personnes, Clients, Conseillers, Administrateurs et Opérations
- [x] Consultations : liste des comptes, liste des clients, liste des opérations d'un compte
- [x] Opérations : retrait, virement et dépôt
- [x] Suppression des comptes bancaires, clients, etc.
- [ ] Modification des comptes bancaires, clients, etc.
- [ ] Connexion & gestion des rôles

## Contraintes implémentées
- [x] Relations de type OneToMany et ManyToMany (voir diagramme UML)
- [x] Utilisation de Primefaces (Voir la page `consulter > comptes bancaires > consulter l'historique des opérations`)
- [x] Utilisation des fonctions d'exports en pdf et csv, d'ordonnancements et de filtrage (Voir la page `consulter > comptes bancaires > consulter l'historique des opérations`)
- [ ] Utilisation de deux types de session beans
- [ ] Utilisation d'au moins deux types de scope dans les backend beans




## Démo

[Vidéo de démonstration](https://youtu.be/h9tRw6HWfXE)
