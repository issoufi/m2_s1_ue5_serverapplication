/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import entities.Customer;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.CustomerManager;

/**
 *
 * @author issoufiadam
 */
@Named(value = "customerMBean")
@ViewScoped
public class CustomerMBean implements Serializable {

    @EJB
    private CustomerManager customerManager;
    
    private List<Customer> customers;

    /**
     * Creates a new instance of CustomerMBean
     */
    public CustomerMBean() {
    }
    
    /**
     * Renvoie la liste des clients pour affichage dans une DataTable
     * @return
     */
    public List<Customer>getCustomers() {
        if (this.customers == null) {
           this.customers = customerManager.getAllCustomers();
        }
        
        return this.customers;
    }

    /**
     * Action handler - appelé lorsque l'utilisateur sélectionne une ligne dans
     * la DataTable pour voir les détails
     */
    public String showDetails(int customerId) {
        return "CustomerDetails?idCustomer=" + customerId;    }
    
    
}
