/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managedbeans;

import entities.Customer;
import entities.DiscountCode;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import session.CustomerManager;
import session.DiscountCodeManager;

/**
 *
 * @author issoufiadam
 */
@Named(value = "customerDetailsMBean")
@ViewScoped
public class CustomerDetailsMBean implements Serializable {
    
    @EJB
    private CustomerManager customerManager;
    @EJB  
    private DiscountCodeManager discountCodeManager;  
    
    private int idCustomer;
    private Customer customer;
    /**
     * Creates a new instance of CustomerDetailsMBean
     */
    public CustomerDetailsMBean() {
    }


    public int getIdCustomer() {
      return idCustomer;
    }

    public void setIdCustomer(int idCustomer) {
      this.idCustomer = idCustomer;
    }

    /**
     * Renvoie les détails du client courant (celui dans l'attribut customer de
     * cette classe), qu'on appelle une propriété (property)
     */
    public Customer getDetails() {
      return customer;
    }

    /**
     * Action handler - met à jour la base de données en fonction du client passé
     * en paramètres, et renvoie vers la page qui affiche la liste des clients.
     */
    public String update() {
      System.out.println("###UPDATE###");
      customer = customerManager.update(customer);
      return "CustomerList";
    }

    /**
     * Action handler - renvoie vers la page qui affiche la liste des clients
     * @return 
     */
    public String list() {
      System.out.println("###LIST###");
      return "CustomerList";
    }

    public void loadCustomer() {
      this.customer = customerManager.getCustomer(idCustomer);
    }

    public Converter getDiscountCodeConverter() {  
        return discountCodeConverter;  
    }
    
    /** 
    * renvoie une liste de DiscountCode pour affichage dans le menu déroulant 
    * de la page des détails d'un client 
    * @return 
    */  
   public List<DiscountCode> getAllDiscountCodes() {  
       return discountCodeManager.getDiscountCodes();  
   } 

    private Converter discountCodeConverter = new Converter() {  

        @Override  
        public Object getAsObject(FacesContext context, UIComponent component, String value) {  
            char code = value.charAt(0);  
            DiscountCode dc = discountCodeManager.getDiscountCodeByDiscountCode(code);  
            return dc;  
       }  
        @Override  
        public String getAsString(FacesContext context, UIComponent component, Object value) {  
            DiscountCode dc = (DiscountCode) value;  
            return dc.getDiscountCode()+" : "+dc.getRate()+"%";   
        }  
    };
}

