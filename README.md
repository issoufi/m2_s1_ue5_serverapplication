# Projet J2EE - Gestion de comptes bancaires

## [Configuration du projet](docs/configuration.md)
* Près-requis
* Télécharger le projet Git
* Lancer le projet

## [Rapport de conception](docs/conception.md)
* Diagramme UML
* Pages réalisées
* Fonctionnalités implémentées
* Contraintes implémentées

## Démo

[Vidéo de démonstration](https://youtu.be/h9tRw6HWfXE)
