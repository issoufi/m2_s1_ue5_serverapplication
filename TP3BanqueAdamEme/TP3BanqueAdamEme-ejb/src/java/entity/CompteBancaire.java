/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

/**
 *
 * @author issoufiadam
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "CompteBancaire.findAll", query = "SELECT cb FROM CompteBancaire cb"), 
    @NamedQuery(name = "CompteBancaire.findByCompteBancaireId", query = "SELECT cb FROM CompteBancaire cb WHERE cb.compteBancaireId = :compteBancaireId")
})
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="compteBancaireId")
    private Long compteBancaireId;
    @Size(max = 30)
    private String nom;
    private int solde;
    /* Un compte bancaire peut avoir plusieurs client. Par exemple un compte joint */
    @ManyToMany(mappedBy="comptes")
    private Collection<Client> clients = new ArrayList<Client>(); 
    /* Un compte bancaire a plusieurs opérations */
    @OneToMany(cascade=ALL, mappedBy="compte")
    private Collection<Operation> operations = new ArrayList<Operation>(); 
    
    public CompteBancaire() {}   

    public CompteBancaire(String nom, int solde, Client client) {
        this.nom = nom;
        this.solde = solde;
        this.clients.add(client);
        this.operations.add(new Operation(solde, "Crédit", this));
    }
    
    public void deposer(int montant) {  
        this.operations.add(new Operation(montant, "Crédit", this));
        this.solde += montant;  
    }
    
    public int retirer(int montant) {  
        if (montant <= solde) {
          this.operations.add(new Operation(-montant, "Débit", this));
          this.solde -= montant;  
          return montant;  
        } else {
          return 0;  
        }  
    }


    public Long getCompteBancaireId() {
        return compteBancaireId;
    }    

    public Collection<Client> getClients() {
        return clients;
    }

    public void setClients(Collection<Client> clients) {
        this.clients = clients;
    }
    
    public void addClient(Client client) {
        this.clients.add(client);
    }

    public Collection<Operation> getOperations() {
        return operations;
    }

    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }
    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getSolde() {
        return solde;
    }

    public void setSolde(int solde) {
        this.solde = solde;
    }

    public Long getId() {
        return compteBancaireId;
    }

    public void setId(Long id) {
        this.compteBancaireId = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compteBancaireId != null ? compteBancaireId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteBancaire)) {
            return false;
        }
        CompteBancaire other = (CompteBancaire) object;
        if ((this.compteBancaireId == null && other.compteBancaireId != null) || (this.compteBancaireId != null && !this.compteBancaireId.equals(other.compteBancaireId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        Client c = (Client) clients.toArray()[0];
        return nom + " / " + c.getNom() + " " + c.getPrenom();
    }
    
}
