/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author issoufiadam
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"), 
    @NamedQuery(name = "Client.findByClientId", query = "SELECT c FROM Client c WHERE c.id = :clientId")
})
public class Client extends Personne {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="clientId", nullable=false)
    private Long id;
    @ManyToMany
    @JoinTable(name="CLIENT_COMPTE")
    private Collection<CompteBancaire> comptes = new ArrayList<CompteBancaire>();
    @ManyToOne
    @JoinColumn(name="conseillerId", nullable=false)
    private Conseiller conseiller;

    public Client() {}
    
    public Client(String nom, String prenom, String adresse) {
        super(nom, prenom, adresse);
    }

    public Collection<CompteBancaire> getComptes() {
        return comptes;
    }

    public void setComptes(Collection<CompteBancaire> comptes) {
        this.comptes = comptes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Conseiller getConseiller() {
        return conseiller;
    }

    public void setConseiller(Conseiller conseiller) {
        this.conseiller = conseiller;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Client[ id=" + id + " ]";
    }
    
}
