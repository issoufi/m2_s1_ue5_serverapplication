/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.util.ArrayList;
import java.util.Collection;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author issoufiadam
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Conseiller.findAll", query = "SELECT c FROM Conseiller c"), 
    @NamedQuery(name = "Conseiller.findByConseillerId", query = "SELECT c FROM Conseiller c WHERE c.id = :conseillerId")
})
public class Conseiller extends Personne {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="conseillerId")
    private Long id;
    @OneToMany(cascade=ALL, mappedBy="conseiller")
    private Collection<Client> clients = new ArrayList<Client>();

    public Conseiller() {}

    public Conseiller(String nom, String prenom, String adresse) {
        super(nom, prenom, adresse);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Collection<Client> getClients() {
        return clients;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conseiller)) {
            return false;
        }
        Conseiller other = (Conseiller) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Conseiller[ id=" + id + " ]";
    }
    
}
