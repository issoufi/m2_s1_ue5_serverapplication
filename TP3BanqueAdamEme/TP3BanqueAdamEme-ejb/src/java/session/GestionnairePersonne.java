/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Administrateur;
import entity.Client;
import entity.Conseiller;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author issoufiadam
 */
@Stateless
@LocalBean
public class GestionnairePersonne {

    @PersistenceContext
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }
    
    // Un client doit être créé par un conseiller. Il faut donc impérativement 
    // désigner un conseiller.
    // Remarque : on doit affecter au client le conseiller qui a créé le compte.
    public Client creerUnClient(String nom, String prenom, String addresse, Conseiller conseiller) {
        Client client = new Client(nom, prenom, addresse);
        client.setConseiller(conseiller);
        
        persist(client);
        return client;
    }
    
    // Un conseiller doit être créé sans avoir de client. On lui affectera un 
    // client à chaque fois qu'il créera un client.
    public Conseiller creerUnConseiller(String nom, String prenom, String addresse) {
        Conseiller conseiller = new Conseiller(nom, prenom, addresse);
        persist(conseiller);
        
        return conseiller;
    }
    
    public Administrateur creerUnAdministrateur(String nom, String prenom, String addresse) {
        Administrateur admin = new Administrateur(nom, prenom, addresse);
        persist(admin);
        
        return admin;
    }
    
    public List<Client> getAllClients() {
        Query query = em.createNamedQuery("Client.findAll");
        return query.getResultList();
    }
    
    public Client getClient(Long compteBancaireId) {
        Query query = em.createNamedQuery("Client.findByClientId");
        query.setParameter("clientId", compteBancaireId);
        return (Client) query.getSingleResult();
    }
    
    public void supprimerClient(Long clientId) {
       Client c = em.find(Client.class, clientId);
       em.remove(c);
    }
 
    public List<Conseiller> getAllConseillers() {
        Query query = em.createNamedQuery("Conseiller.findAll");
        return query.getResultList();
    }
    
    public Conseiller getConseiller(Long conseillerId) {
        Query query = em.createNamedQuery("Conseiller.findByConseillerId");
        query.setParameter("conseillerId", conseillerId);
        return (Conseiller) query.getSingleResult();
    }
    
    public void supprimerConseiller(Long conseillerId) {
       Conseiller c = em.find(Conseiller.class, conseillerId);
       em.remove(c);
    }

    public List<Administrateur> getAllAdministrateurs() {
        Query query = em.createNamedQuery("Administrateur.findAll");
        return query.getResultList();
    }
    
    public Administrateur getAdministrateur(Long administrateurId) {
        Query query = em.createNamedQuery("Conseiller.findByAdministrateurId");
        query.setParameter("administrateurId", administrateurId);
        return (Administrateur) query.getSingleResult();
    }
    
    public void supprimerAdministrateur(Long administrateurId) {
       Administrateur a = em.find(Administrateur.class, administrateurId);
       em.remove(a);
    }
}
