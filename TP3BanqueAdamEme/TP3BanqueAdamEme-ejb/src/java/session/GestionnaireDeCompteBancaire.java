/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Administrateur;
import entity.Client;
import entity.CompteBancaire;
import entity.Conseiller;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author issoufiadam
 */
@Stateless
@LocalBean
public class GestionnaireDeCompteBancaire implements Serializable {

    @PersistenceContext
    private EntityManager em;
    @EJB
    private GestionnairePersonne gestionnairePersonne;
    
    // Les types de compte bancaire
    public static final String COMPTE_COURANT = "Compte Courant";
    public static final String COMPTE_EPARGNE = "Compte Epargne";

    public void creerCompte(CompteBancaire c) {
        persist(c);
    }
    
    public CompteBancaire creerCompte(String nom, int solde, Client client) {
        CompteBancaire c = new CompteBancaire(nom, solde, client);
        persist(c);
        
        return c;
    }
    
    public void creerComptesTest() {
        
        // Création des administrateurs
        Administrateur adminA = gestionnairePersonne.creerUnAdministrateur("Zoro", "Gonzaless", "3 boulvard Napoléon 94000 Paris");
        Administrateur adminB = gestionnairePersonne.creerUnAdministrateur("Mbape", "Mamadou", "2 travers du roi 13003 Marseille");
        
        // Création des conseillers
        Conseiller conseillerA = gestionnairePersonne.creerUnConseiller("John", "Lennon", "2400 route des Dolines 06560 valbonne");
        Conseiller conseillerB = gestionnairePersonne.creerUnConseiller("Paul", "McCartney", "96 avenue Valrose 06100 Nice");
        Conseiller conseillerC = gestionnairePersonne.creerUnConseiller("Ringo", "Starr", "17 rue la belle vie 06360 Antibes");
        Conseiller conseillerD = gestionnairePersonne.creerUnConseiller("Georges", "Harrisson", "200 route de Grace 06780 Antibes");
        
        // Création des clients
        Client ca = gestionnairePersonne.creerUnClient("Adam", "Issoufi", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cb = gestionnairePersonne.creerUnClient("Jack", "Lennon", "2255 route des Dolines 06560 Valbonne", conseillerB);
        Client cc = gestionnairePersonne.creerUnClient("Semon", "McCartney", "2255 route des Dolines 06560 Valbonne", conseillerC);
        Client cd = gestionnairePersonne.creerUnClient("Leonard", "Starr", "2255 route des Dolines 06560 Valbonne", conseillerD);
        Client ce = gestionnairePersonne.creerUnClient("Pepin", "Harrisson", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cf = gestionnairePersonne.creerUnClient("Loulou", "Lennon", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cg = gestionnairePersonne.creerUnClient("Richard", "McCartney", "2255 route des Dolines 06560 Valbonne", conseillerC);
        Client ch = gestionnairePersonne.creerUnClient("George", "Starr", "2255 route des Dolines 06560 Valbonne", conseillerD);
        Client ci = gestionnairePersonne.creerUnClient("Patrick", "Harrisson", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cj = gestionnairePersonne.creerUnClient("Lion", "McCartney", "2255 route des Dolines 06560 Valbonne", conseillerC);
        Client ck = gestionnairePersonne.creerUnClient("Heho", "Starr", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cl = gestionnairePersonne.creerUnClient("Hero", "Harrisson", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cm = gestionnairePersonne.creerUnClient("rolf", "hegdal", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cn = gestionnairePersonne.creerUnClient("Laura", "Gerome", "2255 route des Dolines 06560 Valbonne", conseillerB);
        Client co = gestionnairePersonne.creerUnClient("Bart", "Sisko", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cp = gestionnairePersonne.creerUnClient("Magi", "Hevret", "2255 route des Dolines 06560 Valbonne", conseillerC);
        Client cq = gestionnairePersonne.creerUnClient("Marge", "Chamir", "2255 route des Dolines 06560 Valbonne", conseillerD);
        Client cr = gestionnairePersonne.creerUnClient("Lisa", "Loulou", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client cs = gestionnairePersonne.creerUnClient("Reanaud", "David", "2255 route des Dolines 06560 Valbonne", conseillerA);
        Client ct = gestionnairePersonne.creerUnClient("Nernard", "barnard", "2255 route des Dolines 06560 Valbonne", conseillerB);
        Client cu = gestionnairePersonne.creerUnClient("Fred", "cilvan", "2255 route des Dolines 06560 Valbonne", conseillerD);
        
        // Création des comptes bancaires
        creerCompte(new CompteBancaire(COMPTE_COURANT, 150000, ca));  
        creerCompte(new CompteBancaire(COMPTE_EPARGNE, 10, ca));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 950000, cb));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 20000, cc));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 100000, cd));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 150000, ce));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 950000, cf));  
        creerCompte(new CompteBancaire(COMPTE_EPARGNE, 10, cf));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 20000, cg));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 100000, ch));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 150000, ci));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 950000, cj)); 
        creerCompte(new CompteBancaire(COMPTE_EPARGNE, 10, cj));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 20000, ck));  
        creerCompte(new CompteBancaire(COMPTE_COURANT, 100000, cl));
        
        // Création de compte bancaires joint
        CompteBancaire compteJointA = creerCompte(COMPTE_COURANT, 950000, cm);
        compteJointA.addClient(cn);
        
        CompteBancaire compteJointB = creerCompte(COMPTE_COURANT, 20000, cu);
        compteJointB.addClient(co);
                
        CompteBancaire compteJointC = creerCompte(COMPTE_COURANT, 20000, cc);
        compteJointC.addClient(cp);
        compteJointC.addClient(cr);
        compteJointC.addClient(cs);
        
        CompteBancaire compteJointD = creerCompte(COMPTE_COURANT, 100000, cd); 
        compteJointD.addClient(cq);
        compteJointD.addClient(ct);
    }
    

    public List<CompteBancaire> getAllComptes() {
        Query query = em.createNamedQuery("CompteBancaire.findAll");
        return query.getResultList();
    }
    
    public CompteBancaire getCompte(Long compteBancaireId) {
        Query query = em.createNamedQuery("CompteBancaire.findByCompteBancaireId");
        query.setParameter("compteBancaireId", compteBancaireId);
        return (CompteBancaire) query.getResultList().get(0);
    }
    
    public void deposer(Long compteBancaireId, int montant) {
        CompteBancaire cb = getCompte(compteBancaireId);
        cb.deposer(montant);
        persist(cb);
    }
    
    public int retirer(Long compteBancaireId, int montant) {  
        CompteBancaire cb = getCompte(compteBancaireId);
        int montantRetire = cb.retirer(montant);
        persist(cb);
        return montantRetire;
    }
    
     public void transferer(Long compteBancaireIdFrom, Long compteBancaireIdTo, int montant){
         int montantRetirer = retirer(compteBancaireIdFrom, montant);
         deposer(compteBancaireIdTo, montantRetirer);
    }
     
    public void supprimer(Long compteBancaireId) {
       CompteBancaire cb = em.find(CompteBancaire.class, compteBancaireId);
       em.remove(cb);
    }

    public void persist(Object object) {
        em.persist(object);
    }
    
    public List<String> getTypes() {
        List<String> types = new ArrayList<String>();
        types.add(COMPTE_COURANT);
        types.add(COMPTE_EPARGNE);
        
        return types;
    }
}
