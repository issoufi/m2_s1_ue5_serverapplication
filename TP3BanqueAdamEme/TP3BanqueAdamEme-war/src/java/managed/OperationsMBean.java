/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import session.GestionnaireDeCompteBancaire;

/**
 *
 * @author issoufiadam
 */
@Named(value = "operationsMBean")
@SessionScoped
public class OperationsMBean implements Serializable {

    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    
    private Long compteDebite;
    private Long compteCredite;
    private int montant;
    
    /**
     * Creates a new instance of operationMBean
     */
    public OperationsMBean() {
    }
    
    public Long getCompteDebite() {
        return compteDebite;
    }

    public void setCompteDebite(Long compteDebite) {
        this.compteDebite = compteDebite;
    }

    public Long getCompteCredite() {
        return compteCredite;
    }
    
    public String getNomDuCompteDebite() {
       return gestionnaireDeCompteBancaire.getCompte(compteDebite).getNom();
    }
    
    public String getNomDuCompteCredite() {
        return gestionnaireDeCompteBancaire.getCompte(compteCredite).getNom();
    }
    

    public void setCompteCredite(Long compteCredite) {
        this.compteCredite = compteCredite;
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }
    
    public String actionStringTransferer(String url) {
        gestionnaireDeCompteBancaire.transferer(this.compteDebite, this.compteCredite, this.montant);
        clean();
        return url + "?faces-redirect=true";
    }
    
    public String actionStringDeposer(String url) {
        gestionnaireDeCompteBancaire.deposer(this.compteCredite, this.montant);
        clean();
        return url + "?faces-redirect=true";
    }
    
    public String actionStringRetirer(String url) {
        gestionnaireDeCompteBancaire.retirer(this.compteDebite, this.montant);
        clean();
        return url + "?faces-redirect=true";
    }
    
    public void clean() {
        this.compteCredite = null;
        this.compteDebite = null;
        this.montant = 0;
    }
}
