/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed;

import entity.Client;
import entity.CompteBancaire;
import entity.Operation;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import session.GestionnaireDeCompteBancaire;
import session.GestionnairePersonne;

/**
 *
 * @author issoufiadam
 */
@Named(value = "compteBancaireMBean")
@SessionScoped
public class CompteBancaireMBean implements Serializable{
   
    @EJB
    private GestionnaireDeCompteBancaire gestionnaireDeCompteBancaire;
    @EJB
    private GestionnairePersonne gestionnairePersonne;
    
    private List<CompteBancaire> comptes = new ArrayList<CompteBancaire>();
    private CompteBancaire compteCourant = null;
    
    // attribut utilisé lors de la création de compte
    private String type;
    private Client client;
    private Long clientId;
    private int montant = 0;
    
    /**
     * Creates a new instance of ListCompteBancaireMBean
     */
    public CompteBancaireMBean() {
        // gestionnaireDeCompteBancaire.creerComptesTest();
    }
    
    public void creerCompteBancaireTest() {
        gestionnaireDeCompteBancaire.creerComptesTest();
    } 
    
    public List<CompteBancaire> getAllComptes() {
        return gestionnaireDeCompteBancaire.getAllComptes();
    }
    
    public Collection<Operation> getAllOperations() {
        if (compteCourant == null) return null;
        return compteCourant.getOperations();
    }
    
    public List<String> getTypes() {
        return gestionnaireDeCompteBancaire.getTypes();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(String clientId) {
        this.client = gestionnairePersonne.getClient(Long.parseLong(clientId, 10));
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }
    
    public void creerCompte(CompteBancaire c) {
        gestionnaireDeCompteBancaire.creerCompte(c);
    }
    
    public CompteBancaire getCompte(Long compteBancaireId) {
        return gestionnaireDeCompteBancaire.getCompte(compteBancaireId);
    }
    
    public CompteBancaire getCompteCourant() {
        return compteCourant;
    }
    
    public String getNom() {
        return (compteCourant == null) ? "Undefined" : compteCourant.getNom();
    }
    
    public String getSolde() {
        return (compteCourant == null) ? "Undefined" : "" + compteCourant.getSolde();
    }
    
    public void setCompteCourant(Long compteBancaireId) {
        System.out.println("Compte courant : " + compteBancaireId);
        compteCourant = gestionnaireDeCompteBancaire.getCompte(compteBancaireId);
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
    

    // Les actions string
    public String actionStringCreer(String url) {
        System.out.println("managed.CompteBancaireMBean.actionStringCreer()");
        if (clientId == null || type == null) return url + "?faces-redirect=true";
        this.client = gestionnairePersonne.getClient(clientId);
        gestionnaireDeCompteBancaire.creerCompte(type, montant, client);
        reset();
        return url + "?faces-redirect=true";
    }
    
    public String actionStringSupprimer(Long compteBancaireId, String url) {
        gestionnaireDeCompteBancaire.supprimer(compteBancaireId);
        return url + "?faces-redirect=true";
    }
    
    public String actionStringConsulter(Long compteBancaireId, String url) {
        compteCourant = gestionnaireDeCompteBancaire.getCompte(compteBancaireId);
        return url + "?faces-redirect=true";
    }
    
    // Other
    public void reset() {
        clientId = null;
        type = null;
        montant = 0;
    }
}
