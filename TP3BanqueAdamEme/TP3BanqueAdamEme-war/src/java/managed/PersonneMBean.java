/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package managed;

import entity.Administrateur;
import entity.Client;
import entity.Conseiller;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import session.GestionnairePersonne;

/**
 *
 * @author issoufiadam
 */
@Named(value = "personneMBean")
@SessionScoped
public class PersonneMBean implements Serializable {

    @EJB
    private GestionnairePersonne gestionnairePersonne;
    
    String nom;
    String prenom;
    String adresse;
    Conseiller conseiller;
    
    /**
     * Creates a new instance of PersonneMBean
     */
    public PersonneMBean() {
    }
    
    //
    public List<Client> getAllClients() {
        return gestionnairePersonne.getAllClients();
    }
    
    public List<Conseiller> getAllConseillers() {
        return gestionnairePersonne.getAllConseillers();
    }
    
    public List<Administrateur> getAllAdministrateurs() {
        return gestionnairePersonne.getAllAdministrateurs();
    }
    
    // Getter & setter
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    
   //
    
    public String actionStringCeerClient(String url) {
        System.out.println("## Création d'un client");
    
        List<Conseiller> list = gestionnairePersonne.getAllConseillers();
        gestionnairePersonne.creerUnClient(nom, prenom, adresse, list.get(0));
        reset();
        return url + "?faces-redirect=true";
    }
    
    public String actionStringCeerConseiller(String url) {
        System.out.println("## Création d'un conseiller");
   
        gestionnairePersonne.creerUnConseiller(nom, prenom, adresse);
        reset();
        return url + "?faces-redirect=true";
    }
    
    public String actionStringCeerAdministrateur(String url) {
        System.out.println("## Création d'un administrateur");
   
        gestionnairePersonne.creerUnAdministrateur(nom, prenom, adresse);
        reset();
        return url + "?faces-redirect=true";
    }
    
    public String actionStringSupprimerClient(Long id, String url) {
        gestionnairePersonne.supprimerClient(id);
        return url + "?faces-redirect=true";
    }
    
    public String actionStringSupprimerConseiller(Long id, String url) {
        gestionnairePersonne.supprimerConseiller(id);
        return url + "?faces-redirect=true";
    }
    
    public String actionStringSupprimerAdministrateur(Long id, String url) {
        gestionnairePersonne.supprimerAdministrateur(id);
        return url + "?faces-redirect=true";
    }
    
    // Others
    private void reset() {
        nom = null;
        prenom = null;
        adresse = null;
    }
}
